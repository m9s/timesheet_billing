# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class Employee(ModelSQL, ModelView):
    _name = 'company.employee'

    activities = fields.Many2Many('company.employee-product.activity',
        'employee', 'activity', 'Activities')

Employee()


class Activity(ModelSQL, ModelView):
    'Activity'
    _name = 'product.activity'
    _description = __doc__

    name = fields.Char('Name', required=True)
    product = fields.Many2One('product.product', 'Product',
            domain=[('type', '=', 'service')], required=True,
            ondelete='RESTRICT')

    def get_rec_name(self, ids, name):
        res = {}
        if not ids:
            return res
        for activity in self.browse(ids):
            res[activity.id] = activity.product.name + ' - ' + activity.name
        return res

Activity()


class EmployeeActivity(ModelSQL, ModelView):
    'Employee Activity'
    _name = 'company.employee-product.activity'
    _description = __doc__

    activity = fields.Many2One('product.activity', 'Activity', required=True,
            ondelete='RESTRICT')
    employee = fields.Many2One('company.employee', 'Employee', required=True,
            ondelete='CASCADE')

EmployeeActivity()

# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Timesheet Billing',
    'name_de_DE': 'Zeiterfassung Abrechnung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds billing on timesheet lines
    ''',
    'description_de_DE': '''
    - Fügt Abrechnung für Zeiterfassungspositionen hinzu.
    ''',
    'depends': [
        'account_invoice_billing_description',
        'company',
        'timesheet',
    ],
    'xml': [
        'billing.xml',
        'company.xml',
        'timesheet.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}

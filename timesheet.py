# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pyson import Eval, Or, Bool, Not
from trytond.pool import Pool


class Line(ModelSQL, ModelView):
    _name = 'timesheet.line'

    editable = fields.Function(fields.Boolean('Editable'), 'get_editable')
    billing_lines = fields.One2Many('account.invoice.billing_line',
            'timesheet_line', 'Billing Lines',
            states={
                'readonly': Not(Bool(Eval('editable')))
            }, depends=['editable'])
    activity = fields.Many2One('product.activity', 'Activity',
            states={
                'readonly': Not(Bool(Eval('editable'))),
                'required': True
            }, depends=['editable'],
            #domain=[(
            #    'id', 'in',
            #       Get(Eval('_parent_employee', {}), 'activities', [])
            #    )]
            )

    def __init__(self):
        super(Line, self).__init__()
        self._error_messages.update({
                'modify_line_billing': 'You cannot modify a timesheet line'
                    'with a billing line that is not in draft state',
                'hour_uom_missing': 'Unit of measure of name "Hour" not found!',
                })

        self.date = copy.copy(self.date)
        if self.date.depends is None:
            self.date.depends = ['editable']
        if 'editable' not in self.date.depends:
            self.date.depends += ['editable']
        if not self.work.states:
            self.date.states = {
                    'readonly': Not(Bool(Eval('editable')))
                    }
        elif 'readonly' not in self.date.states:
            self.date.states['readonly'] = Not(Bool(Eval('editable')))
        else:
            self.date.states['readonly'] = Or(Not(Bool(Eval('editable'))),
                    self.date.states['readonly'])

        self.work = copy.copy(self.work)
        if not self.work.states:
            self.work.states = {
                    'readonly': Not(Bool(Eval('editable')))
                    }
        elif 'readonly' not in self.work.states:
            self.work.states['readonly'] = Not(Bool(Eval('editable')))
        else:
            self.work.states['readonly'] = Or(Not(Bool(Eval('editable'))),
                    self.work.states['readonly'])
        if self.work.depends is None:
            self.work.depends = ['editable']
        elif 'editable' not in self.work.depends:
            self.work.depends += ['editable']

        self.description = copy.copy(self.description)
        if not self.description.states:
            self.description.states = {
                    'readonly': Not(Bool(Eval('editable')))
                    }
        elif 'readonly' not in self.description.states:
            self.description.states['readonly'] = Not(Bool(Eval('editable')))
        else:
            self.description.states['readonly'] = Or(Not(Bool(Eval('editable'))),
                    self.description.states['readonly'])
        if self.description.depends is None:
            self.description.depends = ['editable']
        elif 'editable' not in self.description.depends:
            self.description.depends += ['editable']

        self.employee = copy.copy(self.employee)
        if not self.employee.states:
            self.employee.states = {
                    'readonly': Not(Bool(Eval('editable')))
                    }
        elif 'readonly' not in self.employee.states:
            self.employee.states['readonly'] = Not(Bool(Eval('editable')))
        else:
            self.employee.states['readonly'] = Or(Not(Bool(Eval('editable'))),
                    self.employee.states['readonly'])
        if self.employee.depends is None:
            self.employee.depends = ['editable']
        elif 'editable' not in self.employee.depends:
            self.employee.depends += ['editable']

        self._reset_columns()

    def default_editable(self):
        return True

    def default_activity(self):
        employee_obj = Pool().get('company.employee')
        if Transaction().context.get('employee'):
            employee = employee_obj.browse(Transaction().context['employee'])
            if len(employee.activities) == 1:
                return employee.activities[0].id
        return False

    def get_editable(self, ids, name):
        res = {}
        for line in self.browse(ids):
            res[line.id] = True
            if line.billing_lines:
                for billing_line in line.billing_lines:
                    if billing_line.state != 'draft':
                        res[line.id] = False
        return res

    def get_work(self, line):
        return line.work

    def get_rec_name(self, ids, name):
        res = {}
        if not ids:
            return res
        for line in self.browse(ids):
            res[line.id] = "%s, %s" % (line.employee.name, line.work.name)
        return res

    def _get_billing_line_defaults(self, line):
        uom_obj = Pool().get('product.uom')

        with Transaction().set_context(language='en_US'):
            hour_uom_ids = uom_obj.search([('name', '=', 'Hour')], limit=1)
        if not hour_uom_ids:
            self.raise_user_error('hour_uom_missing')
        hour_uom = uom_obj.browse(hour_uom_ids[0])

        res = {}
        work = self.get_work(line)
        work_obj = Pool().get(work._model_name)
        res['work'] = work
        res['party'] = work_obj.get_invoicing_party(work)
        res['from_uom'] = hour_uom
        res['product'] = line.activity.product
        return res

    def _get_billing_lines_from_timesheet_line(self, line):
        defaults = self._get_billing_line_defaults(line)
        work_obj = Pool().get(defaults['work']._model_name)
        res = work_obj._compute_billing_line_vals(line, defaults)
        return res

    def create(self, vals):
        billing_line_obj = Pool().get('account.invoice.billing_line')

        new_id = super(Line, self).create(vals)

        line = self.browse(new_id)
        billing_lines = self._get_billing_lines_from_timesheet_line(line)
        if not billing_lines:
            return new_id
        for billing_line in billing_lines:
            billing_line['timesheet_line'] = new_id
            billing_line_obj.create(billing_line)
        return new_id

    def check_modify(self, ids, raise_exception=False):
        '''
        Check if the lines can be modified
        '''
        if isinstance(ids, (int, long)):
            ids = [ids]
        for line in self.browse(ids):
            if line.billing_lines:
                for billing_line in line.billing_lines:
                    if not billing_line.state == 'draft':
                        if raise_exception:
                            self.raise_user_error('modify_line_billing')
                        return False
        return True

    def write(self, ids, vals):
        billing_line_obj = Pool().get('account.invoice.billing_line')

        # FIXME: This clause can be optimized when issue #1452 is fixed
        # The exceptions should be raised then
        # Then maybe check_modify() could be optimized too.
        if not self.check_modify(ids, raise_exception=False):
            return False
        res = super(Line, self).write(ids, vals)
        if not ids:
            return True
        if isinstance(ids, (int, float)):
            ids = [ids]
        for timesheet_line in self.browse(ids):
            if timesheet_line.billing_lines:
                if self._check_update_billing_lines(vals):
                    billing_lines_to_delete = []
                    for billing_line in timesheet_line.billing_lines:
                        if billing_line.state == 'draft':
                            billing_lines_to_delete.append(billing_line.id)
                    if not billing_lines_to_delete:
                        continue
                    billing_line_obj.delete(billing_lines_to_delete)
                    billing_vals = self._get_billing_lines_from_timesheet_line(
                            timesheet_line)
                    for values in billing_vals:
                        values['timesheet_line'] = timesheet_line.id
                        billing_line_obj.create(values)
        return res

    def _check_update_billing_lines(self, vals):
        for field in ['hours', 'description', 'activity', 'work']:
            if field in vals:
                return True
        return False

    def delete(self, ids):
        self.check_modify(ids, raise_exception=True)
        return super(Line, self).delete(ids)

Line()


class Work(ModelSQL, ModelView):
    _name = 'timesheet.work'

    def get_invoicing_party(self, work):
        # Every model inherited from 'timesheet.work' should implement such
        # a method
        return False

    def _compute_billing_line_vals(self, line, defaults):
        return []

Work()
